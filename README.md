# Programacion declarativa y funcional
## 1. Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .salmon {
    BackgroundColor LightSalmon
  }
  .lime {
    BackgroundColor Lime
  }
  .orchid {
    BackgroundColor Orchid
  }
  .crimson {
    BackgroundColor Crimson
    Fontcolor white
  }
  .gold {
    BackgroundColor Gold
  }
  .orange {
    BackgroundColor Orange
  }
  .cyan {
    BackgroundColor lightcyan
  }
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
}
</style>
* Programación declarativa <<orchid>>
 *_ es
  * un estilo de programacion <<orange>>
   *_ surge como
    * reacción a los problemas de\nla programacion clásica
   *_ hace
    * un énfasis en los aspectos\ncreativos de la programación <<crimson>>
 *_ proposito
  * liberarse de las asignaciones <<orange>>
   *_ y
    * los detalles sobre la gestión\nde memoria <<crimson>>
  * llegar a un nivel\nmas alto <<orange>>
   *_ cercano
    * a la forma de pensar del\nprogramador
 * ventaja
  *_ hacer
   * programas mas cortos <<orange>>
    * más faciles de depurar
     *_ y
      * de realizar
 * variantes principales
  * programación funcional <<lime>>
   *_ recurre
    * al lenguaje matematico <<blue>>
     *_ para
      * describir funciones
   *_ aplica
    * funciones a datos complejos <<blue>>
   * caracteristicas
    * funciones de orden superior <<blue>>
     *_ consiste en
      * que la entrada de un\nprograma puede ser otro\nprograma <<gold>>
    * evaluación perezosa <<blue>>
     *_ consiste en
      * evaluar aquello que es\nestrictamente necesario <<gold>>
       *_ para
        * hacer calculos posteriores\nque requiere el programa <<gold>>
   * ejemplo
    * serie de fibonacci <<blue>>
   * lenguajes
    * Lisp <<blue>>
     *_ es
      * un lenguaje de programación
       *_ que tiene
        * relación con la programación\nfuncional <<gold>>
    * Haskell <<blue>>
     *_ es
      * un lenguaje de programación\nfuncional
  * programación lógica <<lime>>
   *_ recurre
    * a la lógica de predicados\nde primer orden
     *_ predicados
      * son relaciones entre\nlos objetos que se\ndefinen <<salmon>>
   *_ no hay distinción
    * entre datos de entrada <<crimson>>
     *_ y
      * datos de salida <<salmon>>
   *_ permite
    * ser mas declarativo <<crimson>>
     *_ esto se debe a
      * que no se establece la\ndireccion de procesamiento <<salmon>>
       *_ entre 
        * argumentos y resultados <<gold>>
   * lenguaje
    * Prolog
     *_ es el
      * lenguaje estándar
       *_ donde
        * se implementa el concepto\nde programacion lógica <<salmon>>
@endmindmap
```
## 2. Lenguaje de Programación Funcional (2015)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .salmon {
    BackgroundColor LightSalmon
  }
  .lime {
    BackgroundColor Lime
  }
  .orchid {
    BackgroundColor Orchid
  }
  .crimson {
    BackgroundColor Crimson
    Fontcolor white
  }
  .gold {
    BackgroundColor Gold
  }
  .orange {
    BackgroundColor Orange
  }
  .cyan {
    BackgroundColor lightcyan
  }
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
}
</style>
* Lenguaje de programación funcional <<orange>>
 *_ un
  * lenguaje dota de semantica\na los programas <<gold>>
   *_ siguiendo
    * un paradigma de programación
     *_ es
      * un modelo de computacion <<salmon>>
 *_ cimientos
  * trabajos sobre lamda cálculo <<gold>>
   *_ luego
    * Haskell Brooks desarrollo\nla lógica combinatoria
     *_ luego
      * en 1965 se modeló un\nlenguaje de programacion <<salmon>>
 * en la programación funcional <<blue>>
  *_ hay
   * funciones en su concepción\npuramente matemática <<gold>>
    *_ ejemplo
     * x^2 = x * x
     * sumar(x,i) = x + i
    *_ estas funciones
     * reciben parametros de entrada
      *_ y
       * devuelven una salida <<blue>>
    *_ consecuencias
     * se pierde el concepto de variable\ncomo una zona de memoria\nque se puede modificar
      *_ y también
       * los conceptos basados en él <<blue>>
        *_ ejemplo
         * los bucles <<salmon>>
     * los bucles deben ser resueltos\nde otra forma
      *_ por la
       * recursión <<blue>>
       * funciones recursivas <<blue>>
        *_ son
         * aquellas que hacen referencia\na si mismas <<salmon>>
  *_ las variables
   * existen pero solamente sirven para <<gold>>
    *_ referirse
     * a los parámetros de entrada\nde las funciones
  *_ las constantes
   * existen pero son equivalentes\na funciones <<gold>>
    *_ cuando
     * una función devuelve siempre\nel mismo resultado
    *_ ejemplo
     * una función que no\nrecibe parámetros
      *_ es
       * una función que siempre devuelve\nel mismo resultado <<lime>>
  * ventajas <<green>>
   *_ los programas
    * solo dependen de los parámetros\nde entrada <<rose>>
   * transparencia referencial
    *_ significa
     * que los resultados de las\nfunciones son independientes <<rose>>
      *_ del
       * orden en el que se realizan\nlos cálculos <<lime>>
   * en un entorno multiprocesador
    *_ se puede
     * paralelizar las funciones\nsin ningún problema <<rose>>
  * caracteristicas <<green>>
   *_ todo
    * gira alrededor de las funciones
   * funciones de orden superior
    *_ son
     * funciones que reciben como\nparámetro otras funciones <<rose>>
   *_ existe 
    * la aplicación parcial
 *_ las funciones
  * se evualuan en la evaluación\nque se conoce <<blue>>
   *_ como
    * evaluación perezosa
     *_ consiste en
      * evaluar aquello que es\nestrictamente necesario <<blue>>
       *_ para
        * hacer los cálculos posteriores\nque requiere el programa <<salmon>>
@endmindmap
```